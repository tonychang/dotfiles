set number relativenumber " Allow hybrid number and relative
set tabstop=4               " Width of a TAB is set to 4.  
set softtabstop=4           " Set the number of columns for a TAB
set expandtab               " expand TABS to spaces
set shiftwidth=4 smarttab   " Indents will have a width of 4 

execute pathogen#infect('bundle/{}')
syntax on

set clipboard=unnamedplus
set laststatus=2

